{config, lib, resources, ...}:
with lib;

let
  cfg = config.deployment.enum;
  alphaSort = data: builtins.sort (a: b: a < b) data;
  machineIds = listToAttrs (imap (i: v: {name = v; value = i;}) (alphaSort (attrNames resources.machines)));
in {
  options = {
    deployment.enum = {
      id = mkOption {
        type = types.int;
        default = machineIds.${config.networking.hostName};
        description = "Enumerated unique machine ID.";
      };
    };
  };
}
